#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""[TODO: application description here]"""

from __future__ import (absolute_import, division, print_function,
                        with_statement)

__authors__ = [
    "muntdefems",
    "Stephan Sokolow (deitarion/SSokolow)"
]
__appname__ = "muntdefems's MaGog parser"
__version__ = "0.0pre1"
__license__ = "unknown (TODO)"

import csv, logging, sys
from itertools import repeat, chain

log = logging.getLogger(__name__)

import urllib, json
import codecs, HTMLParser
import difflib
import datetime as dt

ChangelogParserFile = 'HTML2BBCODE.txt'

useful_fields = ['id', 'title', 'link', 'genres', 'file_size', 'rls_date',
                 'os', 'languages', 'companies', 'features', 'bonuses',
                 'cur_price', 'age_req', 'is_dlc', 'sys_req_win',
                 'sys_req_mac', 'add_date', 'is_season', 'is_premium',
                 'is_upcoming', 'is_removed', 'forum', 'forum_tbd',
                 'sys_req_linux', 'missing_game', 'title_fr', 'price_hist',
                 'downloadables', 'compatibility', 'title_de', 'all_files',
                 'missing_dlcs', 'num_unbundles', 'unbundles_str', 'title_ru',
                 'regional_full', 'reg_price_full', 'is_indev', 'is_demo',
                 'library_id', 'changelog_time', 'dl_has_changelog']

# Define this once so the reader and writer can't get out of sync
CHANGELOG_SEPARATOR = '---------------------'

# Shared definitions for stuff references multiple times
INDENT = u'\u00a0' * 4
SECTION_SEPARATOR = '---------------------------\n'

#############
# Functions #
#############

class MagogDumpDialect(csv.Dialect):
    """A CSV dialect definition for magog dump files"""
    delimiter = '|'
    escapechar = None
    lineterminator = '\n'
    quoting = csv.QUOTE_NONE
    # TODO: Ask mrkgnao to verify that I've interpreted the format correctly

def read_magog_file(path):
    """Parse a magog data dump into a dict of dicts.

    Format: {str(id): {...}, ...}
    """
    games = {}
    with codecs.open(path, 'r', encoding='utf-8') as fobj:
        # Since DictReader doesn't support multi-character delimiters and
        # mrkgnao seems to not use any quoting, we use a generator expression
        # as a simple adapter
        for row in csv.DictReader(
                (line.replace(' | ', '|') for line in fobj),
                dialect=MagogDumpDialect):
            games[row['id']] = {f: row[f] for f in useful_fields}

    return games

def field_count(rows_dict):
    """Return the number of items in a row (any row) if a dict of lists.

    - Assumes something like DictReader will have ensured equal-length rows
    - Broken out from its point of use for clarity
    """
    # This is the cleanest way to be both efficient and Py2/Py3 compatible
    for key in rows_dict:
        return len(rows_dict[key])

def read_changelog_file(path):
    """Parse changelog files into {id:changelog} dicts.

    Algorithm:
    1. Split the file into a list using the separator line as a delimiter
    2. Using list slicing, de-interleave the IDs and content blocks into two
       parallel lists
    3. Use zip(ids, contents) to get a list of (id, content) tuples
       (zip converts a list of columns into a list of rows or vice-versa)
    4. Convert the list of 2-tuples into a dict, using a dict comprehension so
       that, in the process, the right mix of strip() and lstrip() can be
       applied to exactly match the output of the old algorithm.)
    """
    with codecs.open(path, 'r', encoding='utf-8') as fobj:
        pieces = fobj.read().split(CHANGELOG_SEPARATOR)

    # TODO: Switch to val.strip() once it's no longer necessary to prove
    #       identical output to the original (IMO flawed) algorithm.
    changelogs = {key.strip(): val.lstrip()
                  for key, val in zip(pieces[0::2], pieces[1::2])}
    return changelogs

def get_id_for_changelog(games, g):
    game = games[g]
    id_list = {}
    if int(game['num_unbundles']) > 0:
        if game['downloadables'] not in ('', None):
            for t in game['unbundles_str'].split(' *** '):
                id_list[t.split(' (')[1][:-1]] = t.split(' (')[0]
#        candidates = game['unbundles_str'].replace(
#            '(','').replace(')','').split()
#        id_list = [s for s in candidates if s.isdigit() and int(s) > 1E9]
    elif game['library_id'] not in ('', game['id']):
        id_list[game['library_id']] = game['title']
#        id_list = [game['library_id']]
    else:
        id_list[g] = game['title']
    return id_list

def retrieve_changelog(i):
    url = 'https://api.gog.com/products/{0}?expand=changelog'.format(i)
    response = urllib.urlopen(url)
    data = json.loads(response.read())
    return data.get('changelog', 'ERROR: Changelog not found')

def read_changelog_parser(File):
    parser_dict = {}
    for line in open(File, 'r').readlines():
        fields = line.strip().split('|')
        parser_dict[fields[0]] = fields[1]
    return parser_dict

def parse_changelog(chlog, parser_dict):
    for html in parser_dict:
        chlog = chlog.replace(html, parser_dict[html])
    return chlog

def find_updated_games(games_old, games_new):
    """Find differences between two parsed magog dumps"""
    added, removed, diff = [], [], {}

    removed = set(games_old) - set(games_new)
    added = set(games_new) - set(games_old)

    for game_id, game in games_new.items():
        if game_id not in games_old:
            continue

        diff_fields = [f for f in game if game[f] != games_old[game_id][f]]
        if diff_fields:
            diff[game_id] = diff_fields
    return added, removed, diff

def read_all_files(games, g):
    files = {}
    for f in games[g]['all_files'][:-2].split(']; '):
        curr_file = {}
        name = f.split('[http')[0].strip()
        name = name[name.find(')') + 2:]
        link, version, size, missing = f.split('[http')[1].split(', ')
        link = 'http' + link
        if len(missing) > 0 and missing[-1] == ']':
            missing = missing[:-1]
        curr_file['link'] = link
        curr_file['version'] = version
        curr_file['size'] = size
        curr_file['missing'] = missing
        files[name] = curr_file
    return files

def read_downloadables(games, g):
    files = {}
    for f in games[g]['downloadables'][:-2].split(';'):
        curr_file = {}
        sep = f.rfind('[')
        name = f[0:sep].strip()
        name = name[name.find(')') + 2:]
        try:
            (file_, date1_, date2_, date3_, size_, link_, version_
             ) = f[sep + 1:-1].split(', ')
        except:
            continue
        curr_file['file'] = file_
        curr_file['date'] = date1_ + ' ' + date2_
        curr_file['size'] = size_
        curr_file['link'] = link_
        curr_file['version'] = version_
        files[name] = curr_file
    return files

# TODO: I think I can make this more elegant
def human_size(num, suffix='B'):
    for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Y', suffix)

def strikethrough(text):
    """Unicode-based replacement for [s][/s]"""
    return ''.join(chain.from_iterable(zip(text, repeat(u'\u0336'))))

# TODO: I need to research a better diffing algorithm because this still
#       produces things like Single{+-player, Multi+}-player which, while
#       accurate, aren't maximally intuitive.
def diff_strings(before, after):
    """Do inline/word-level diffing and return an annotated string"""
    output = []
    matcher = difflib.SequenceMatcher(None, before, after)
    for tag, old_start, old_stop, new_start, new_stop in matcher.get_opcodes():
        old_text = before[old_start: old_stop]
        new_text = after[new_start: new_stop]

        old_text = strikethrough(old_text)
        if tag == 'equal':
            output.append(new_text)
        else:
            if tag == 'insert':
                change = u'[i]{{+[/i]{0}[i]+}}[/i]'.format(new_text)
            elif tag == 'delete':
                change = old_text
            elif tag == 'replace':
                change = (u'[i]([/i]{0}[i] \u2192 [/i]{1}[i])[/i]'
                          ).format(old_text,new_text)
            else:
                change = '?{}?'.format(new_text)
                logging.error('get_opcodes() should never return tag %r', tag)
            output.append('[b]{}[/b]'.format(change))
    return ''.join(output)


####################################

def process_logs(InFile1, InFile2, ChangelogsFile,
                 OutputFile, ChangelogsFileUpdated):
    OutputString = ''
    games1 = read_magog_file(InFile1)
    games2 = read_magog_file(InFile2)

    nf1, nf2 = field_count(games1), field_count(games2)
    if nf1 != nf2:
        sys.exit('The number of fields in file {0} ({1}) and file {2} ({3}) '
                 'are different.'.format(InFile1, nf2, InFile2, nf2))

    Changelogs = read_changelog_file(ChangelogsFile)
    ChangelogsChanged = False

    chlog_parser = read_changelog_parser(ChangelogParserFile)

    added_games, removed_games, diff_games = find_updated_games(games1, games2)

    def grouped_list(entries, header, sort_key_cb=lambda x: x['title'],
                     format_cb=lambda x: x['title']):
        output = ''
        if entries:
            output += '[b][u]{}[/u][/b]\n'.format(header)
            # TODO: Use humansort for this
            for entry in sorted(entries, key=sort_key_cb):
                output += '{}- {}\n'.format(INDENT, format_cb(entry))
        return output + '\n'

    OutputString += grouped_list([games1[x] for x in removed_games],
        'The following games have probably been removed')
    OutputString += grouped_list([games2[x] for x in added_games],
        'The following games have probably been added')

    for g in sorted(diff_games.keys()):
        # apanyo temporal a eliminar!!!
        if 'cur_price' in diff_games[g]:
            diff_games[g].remove('cur_price')
        if len(diff_games[g]) == 0:
            continue

        # REGIONAL PRICES
        # TODO: This looks like it'd cause the system to flat-out ignore
        #   changes of all kinds (eg. new files) on regionally-priced games.
        #   Am I reading that right and, if so, why?
        if ('regional_full' in diff_games[g] or
                'reg_price_full' in diff_games[g] or
                'reg_price' in diff_games[g]):
            continue

        OutputString += SECTION_SEPARATOR + '\n'
        OutputString += '[b][u]{0}[/u][/b] has probably changed\n\n'.format(
            games2[g]['title'])
        # OutputString += 'Affected fields are: {0}'.format(diff_games[g])

        output_temp = [] # TODO: Just temporary until I finish refactoring
        def format_meta_notification(field_name, action):
            output_temp.append('>>> [u]{0}[/u] {1}\n\n'.format(
                field_name, action))

        def format_meta_changes(field_id, field_name, do_diff_strings=True):
            if field_id in diff_games[g]:
                m1, m2 = games1[g][field_id], games2[g][field_id]
                if not m1.strip():
                    change_str = '[b]added[/b]: {}'.format(m2)
                elif not m2.strip():
                    change_str = '[b]removed[/b]: {}'.format(strikethrough(m2))
                else:
                    if do_diff_strings:
                        change_str = 'changed: {}'.format(diff_strings(m1, m2))
                    else:
                        change_str = ('changed [b]FROM[/b] [i]{0}[/i] '
                                      '[b]TO[/b] [i]{1}[/i]'.format(m1, m2))

                format_meta_notification(field_name, change_str)

        # TODO: Choose an approach to producing output (pure vs. side-effects)
        #       and make this and format_meta_notification both use it.
        def format_file_notification(field_name, text):
            # TODO: Figure out how to pad with nbsp directly
            padded_name = '{:_>18}'.format('[u]' + field_name + '[/u]'
                                           ).replace('_', u'\u00a0')
            return u'\n{0}--- {1}: {2}'.format(
                INDENT, padded_name, text)

        # FILE CHANGES
        format_meta_changes('title', 'Game title')
        format_meta_changes('os', 'Supported OS')
        format_meta_changes('sys_req_linux', 'System requirements for Linux')
        format_meta_changes('sys_req_mac', 'System requirements for OS X')
        format_meta_changes('sys_req_win', 'System requirements for Windows')
        format_meta_changes('languages', 'Supported languages')
        format_meta_changes('compatibility', 'Compatibility')
        format_meta_changes('features', 'Features')
        format_meta_changes('bonuses', 'Bonuses')

        if 'forum_tbd' in diff_games[g]:
            if games1[g]['forum_tbd'] == '1' and games2[g]['forum_tbd'] == '0':
                format_meta_notification('Sub-forum',
                    'added: [url={0}]{1} sub-forum[/url]').format(
                        games2[g]['forum'], games2[g]['title'])


        # TODO: Just temporary until I finish refactoring
        OutputString += ''.join(output_temp)

        FileChanges = False
        files1, files2 = {}, {}

        def format_file_add(file, field_id, field_name, format_cb=lambda x: x):
            if files2[file].get(field_id, '') != '':
                return format_file_notification(
                    field_name, format_cb(files2[file][field_id]))
            else:
                return ''

        def format_file_changes(file, field_id, field_name,
                                format_cb=lambda x: x):
            before = files1[file].get(field_id, '')
            after = files2[file].get(field_id, '')
            if (before != after):
                return format_file_notification(field_name,
                    '[i]{0}[/i] [b]→[/b] [i]{1}[/i]').format(
                        strikethrough(format_cb(before)), format_cb(after))
            else:
                # TODO: Group these into a single line
                return format_file_notification(
                    field_name, 'has not changed')


        if ('downloadables' in diff_games[g] and
                games1[g]['downloadables'] != '' and
                games2[g]['downloadables'] != ''):
            files1 = read_downloadables(games1, g)
            files2 = read_downloadables(games2, g)
        elif ('all_files' in diff_games[g] and
                games1[g]['all_files'] != '' and
                games2[g]['all_files'] != ''):
            files1 = read_all_files(games1, g)
            files2 = read_all_files(games2, g)
        if len(files1) > 0 and len(files2) > 0:
            added_f = []
            removed_f = []
            changed_f = []
            for f1 in sorted(files1):
                if (f1 not in files2 and
                        f1 + ' [Windows]' not in files2 and
                        f1.replace(' [Windows]', '') not in files2):
                    removed_f.append(f1)
            for f2 in sorted(files2):
                if (f2 + ' [Windows]' in files1 or
                        (' [Windows]' in f2 and
                            f2.replace(' [Windows]', '') in files1)):
                    continue
                elif f2 not in files1:
                    added_f.append(f2)
                else:
                    if files1[f2] != files2[f2]:
                        changed_f.append(f2)
            if added_f or removed_f or changed_f:
                OutputString += '\n[b][FILE UPDATES][/b]\n\n'
                FileChanges = True

            if removed_f:
                OutputString += ' * [b]Files removed:[/b]\n'
                for f in removed_f:
                    OutputString += '{0}- {1}\n'.format(INDENT, f)
                OutputString += '\n'
            for f in added_f:
                message = ' * [b]File added[/b]: {0}'.format(f)
                message += format_file_add(f, 'file', 'File name')
                message += format_file_add(f, 'size', 'File size',
                                           lambda x: human_size(int(x)))
                message += format_file_add(f, 'date', 'Date')
                message += format_file_add(f, 'version', 'Version')
                OutputString += message + '\n\n'

            for f in changed_f:
                message = ' * [b]File changed[/b]: {0}'.format(f)
                message += format_file_changes(f, 'file', 'File name')
                message += format_file_changes(f, 'size', 'File size',
                                               lambda x: human_size(int(x)))
                message += format_file_changes(f, 'date', 'Date')
                message += format_file_changes(f, 'version', 'Version')

                # additionals = [files1[f]['missing'], files2[f]['missing']]
                # if additionals[0] != '' and additionals[1] != '':
                #     message += (' --- Note: [b]FROM[/b] {0} [b]TO[/b] {1}'
                #       ).format(additionals[0], additionals[1])
                # elif additionals[0] != '':
                #     message += ' --- No longer {0}'.format(additionals[0])
                # elif additionals[1] != '':
                #     message += ' --- Now {0}'.format(additionals[1])
                OutputString += message + '\n\n'

        if 'changelog_time' in diff_games[g]:
            if 'dl_has_changelog' in diff_games[g]:
                if (games1[g]['dl_has_changelog'] == '1' and
                        games2[g]['dl_has_changelog'] == '0'):
                    OutputString += '\n[b][CHANGELOG REMOVED][/b]\n\n'
                elif (games1[g]['dl_has_changelog'] == '0' and
                      games2[g]['dl_has_changelog'] == '1'):
                    OutputString += '\n[b][CHANGELOG ADDED][/b]\n\n'
                    id_dic = get_id_for_changelog(games2, g)
                    for cl_id in id_dic:
                        if len(id_dic) > 1:
                            OutputString += 'Changelog for {0}\n'.format(
                                id_dic[cl_id])
                        cl_content = retrieve_changelog(cl_id)
                        OutputString += parse_changelog(cl_content,
                                                        chlog_parser)
            else:
                OutputString += '\n[b][CHANGELOG UPDATED][/b]\n\n'
                id_dic = get_id_for_changelog(games2, g)
                for cl_id in id_dic:
                    if len(id_dic) > 1:
                        OutputString += 'Changelog for {0}\n'.format(
                            id_dic[cl_id])
                    old_cl = Changelogs[cl_id].strip()
                    new_cl = retrieve_changelog(cl_id)
                    if new_cl != old_cl:
                        if len(old_cl) > len(new_cl):
                            OutputString += ('\nERROR: The new changelog is '
                                             'shorter than the old one.\n\n')
                        else:
                            minlen = min(len(old_cl), len(new_cl))
                            c = -1
                            while new_cl[c] == old_cl[c] and abs(c) < minlen:
                                c -= 1
                            diff_cl = new_cl[:c].strip()
                            OutputString += parse_changelog(diff_cl,
                                                            chlog_parser)
                            Changelogs[cl_id] = new_cl
                            ChangelogsChanged = True
        elif FileChanges:
            OutputString += '\n[b][NO CHANGELOG][/b]\n\n'

    OutputString += SECTION_SEPARATOR

    # Open the output file and write the output string to it
    with codecs.open(OutputFile, 'w', encoding='utf-8') as fout:
        fout.write(HTMLParser.HTMLParser().unescape(OutputString))
    fout.close()

    # If a changelog has changed, save all changelogs in a new file
    if ChangelogsChanged:
        with codecs.open(ChangelogsFileUpdated, 'w', encoding='utf-8'
                         ) as f_chl:
            for i in Changelogs:
                if Changelogs[i] is not None and Changelogs[i] != '':
                    f_chl.write(i)
                    f_chl.write('\n' + CHANGELOG_SEPARATOR + '\n')
                    f_chl.write(Changelogs[i])
                    f_chl.write('\n')
                    f_chl.write(CHANGELOG_SEPARATOR + '\n')
        f_chl.close()

def main():
    """The main entry point, compatible with setuptools entry points."""
    # If we're running on Python 2, take responsibility for preventing
    # output from causing UnicodeEncodeErrors. (Done here so it should only
    # happen when not being imported by some other program.)
    if sys.version_info.major < 3:
        reload(sys)
        sys.setdefaultencoding('utf-8')  # pylint: disable=no-member

    current_datetime = dt.datetime.strftime(dt.datetime.now(), '%Y%m%d-%H%M%S')

    from argparse import ArgumentParser, RawTextHelpFormatter
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter,
            description=__doc__.replace('\r\n', '\n').split('\n--snip--\n')[0])
    parser.add_argument('--version', action='version',
            version="%%(prog)s v%s" % __version__)
    parser.add_argument('-v', '--verbose', action="count",
        default=2, help="Increase the verbosity. Use twice for extra effect.")
    parser.add_argument('-q', '--quiet', action="count",
        default=0, help="Decrease the verbosity. Use twice for extra effect.")

    parser.add_argument('InFile1', help="TODO: Description")
    parser.add_argument('InFile2', help="TODO: Description")
    parser.add_argument('ChangelogsFile', help="TODO: Description")

    parser.add_argument('-o', '--outfile',
        default='Output_' + current_datetime + '.txt',
        help="Set the filename for the generated output. "
             "(Default: %(default)s)")
    parser.add_argument('-c', '--changelogs_updated',
        default='CHANGELOGS_NEWNEWNEWNEW.txt',
        help="Set the filename where updated changelogs will be stored if "
             "changes are detected. (Default: %(default)s)")

    args = parser.parse_args()

    # Set up clean logging to stderr
    log_levels = [logging.CRITICAL, logging.ERROR, logging.WARNING,
                  logging.INFO, logging.DEBUG]
    args.verbose = min(args.verbose - args.quiet, len(log_levels) - 1)
    args.verbose = max(args.verbose, 0)
    logging.basicConfig(level=log_levels[args.verbose],
                        format='%(levelname)s: %(message)s')

    process_logs(args.InFile1, args.InFile2, args.ChangelogsFile,
                 args.outfile, args.changelogs_updated)

if __name__ == '__main__':
    main()

# vim: set sw=4 sts=4 expandtab :
